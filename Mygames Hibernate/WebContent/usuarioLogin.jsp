<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% if(session.getAttribute("UsuarioNOMBRE") != null){
	response.sendRedirect("index.do");	
}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
	<style type="text/css">
		body {
			padding-top: 200px;
			padding-bottom: 40px;
			background-color: #f5f5f5;
		}
		
		.form-signin {
			max-width: 300px;
			padding: 19px 29px 29px;
			margin: 0 auto 20px;
			background-color: #fff;
			border: 1px solid #e5e5e5;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
			-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
			box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row span4 offset4" style="text-align:center">
			<form class="form-signin" action="usuarioLogin" method="POST">
				<h2 class="form-signin-heading">Login</h2>
				<input type="text" class="input-block-level" name="user" placeholder="Usuario">
				<input type="password" class="input-block-level" name="pass" placeholder="Password">
				<input class="btn btn-primary" type="submit" value="Entrar"/>
				<a class="btn btn-danger" href="registro.php">Regístrate</a>
				<%if(request.getAttribute("error_login") != null){%>
					<p><div class="alert alert-error">${error_login}</div></p>
				<%}%>
			</form>
		</div>
	</div>
</body>
</html>