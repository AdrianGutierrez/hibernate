<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<%--<% if(session.getAttribute("UsuarioNOMBRE") == null){ 
	response.sendRedirect("usuarioLogin");	
 }--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Bienvenido a Moory</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
	<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
		
</head>
<body>
	<div class="contenedor">
		<div class="masthead">
			<ul class="nav nav-pills pull-right">
				<li class="active"><a href="#"><i class="icon-home"></i>Inicio</a></li>
				<li><a href="mailto:lope.diez.mario@gmail.com"><i class="icon-envelope"></i>Contacto</a></li>
				<li><a href="usuarioLogout"><i class="icon-off"></i>Cerrar sesión</a></li>
			</ul>
			<h3 class="muted">Bienvenido ${UsuarioNOMBRE}</h3>
		</div>

		<div id="wrapper">
			<h2>Productos</h2>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Fecha de lanzamiento</th>
						<th>Distribuidor</th>
						<th>Sistema</th>
						<th>Puntuacion</th>
						<th>Nº jugadores</th>
						<th>Editar</th>
						<th>Borrar</th>
						</tr>
				</thead>
				<tbody>
					<c:forEach var="videojuego" items="${listaDeVideojuegos}">
					<tr>
						<td>${videojuego.title}</td>
						<td>${videojuego.release_date}</td>
						<td>${videojuego.publisher}</td>
						<td>${videojuego.system}</td>
						<td>${videojuego.rating}</td>
						<td>${videojuego.num_players}</td>
						
						<td><a class="btn btn-info btn-small" href="editar.do?">
							<i class="icon-pencil icon-white"></i> Editar</a>
						</td>
						<td><a class="btn btn-info btn-small" href="eliminar.php?">
							<i class="icon-trash icon-white"></i> Eliminar</a>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<a class="btn btn-small btn-success" href="agregar.php"><i
				class="icon-plus icon-white"></i> Añadir nuevos datos</a>
		</div>
		

	</div>
</body>
</html>