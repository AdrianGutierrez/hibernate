package videojuegos.adrian;

import java.io.Serializable;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@SuppressWarnings("serial")
public class Usuario implements Serializable {
	
	private int ID;
	private String user;
	private String passwd;
	
	public Usuario(){
		super();
	}

	/**
	 * @param user
	 * @param password
	 */
	public Usuario(int ID, String user, String passwd) {
		this.ID = ID;
		this.user = user;
		this.passwd = passwd;
	}
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the passwd
	 */
	public String getPasswd() {
		return passwd;
	}

	/**
	 * @param password the passwd to set
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public static Usuario buscar(String user, String pass)  {
		SessionFactory factoriaSession = HibernateHelper.getSessionFactory();
		System.out.println("--> Abrimos sesión");
		 
		
		Session session = factoriaSession.openSession();
		
		
		String query = "SELECT p FROM Usuario p WHERE p.user = :usuario AND p.passwd = :contrasena";
		//String query = "SELECT p FROM Usuario p WHERE p.user = 'adrian' AND p.passwd = 'adrian'";
		System.out.println("--> "+ user);
		System.out.println("--> "+ pass);
		System.out.println("--> Hacemos consulta");
		
		Query q = session.createQuery(query).setParameter("usuario", user).setParameter("contrasena", pass);
		//Query q = session.createQuery(query);
		
		System.out.println("--> "+ q);
		
		Usuario usuario = (Usuario)q.uniqueResult();
		
		System.out.println("--> " + usuario);
		session.close();
		
		return usuario;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Usuario [user=" + user + ", password=" + passwd + "]";
	}
}
