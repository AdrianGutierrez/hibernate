package videojuegos.adrian;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControladorVideojuegos extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

	}
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher despachador = null;

		if (request.getServletPath().equals("/index.do")) {
			List<Videojuego> listaDeVideojuegos = Videojuego.buscarTodos();
			System.out.println(listaDeVideojuegos.get(0).getId()); //control
			request.setAttribute("listaDeVideojuegos", listaDeVideojuegos);
			despachador = request.getRequestDispatcher("index.jsp");

		} else if (request.getServletPath().equals("/editar.do")) {

			int id = Integer.valueOf(request.getParameter("id"));
			Videojuego videojuego = Videojuego.buscarPorId(id);
			request.setAttribute("videojuego", videojuego);

			despachador = request.getRequestDispatcher("videojuegoEditar.jsp");

		} else if (request.getServletPath().equals("/modificarVideojuego.do")) {
			
			int id = Integer.valueOf(request.getParameter("id"));
			String title = request.getParameter("title");
			String release_date = request.getParameter("release_date");
			String publisher = request.getParameter("publisher");
			String system = request.getParameter("system");
			String rating = request.getParameter("rating");
			String num_players = request.getParameter("num_players");
			SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
			Date fechapublicacion = null;
			try {
				fechapublicacion = fecha.parse(release_date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Videojuego videojuego = new Videojuego(id, title, fechapublicacion, publisher, system, Integer.valueOf(rating), Integer.valueOf(num_players));
			
			boolean error = videojuego.validar(request);
			
			if(error == false){
				videojuego.actualizar();
				despachador = request.getRequestDispatcher("index.jsp");
				
			}else{
				despachador = request.getRequestDispatcher("videojuegoEditar.jsp");
			}
			
		} 
		despachador.forward(request, response);		
	}
}
