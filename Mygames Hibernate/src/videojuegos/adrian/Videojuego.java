package videojuegos.adrian;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.servlet.http.HttpServletRequest;


public class Videojuego implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private Date release_date;
	private String publisher;
	private String system;
	private int rating;
	private int num_players;
	
	
	public Videojuego() {
	}

	public Videojuego(String title, Date release_date, String publisher,
			String system, int rating, int num_players) {
		this.title = title;
		this.release_date = release_date;
		this.publisher = publisher;
		this.system = system;
		this.rating = rating;
		this.num_players = num_players;
		
	}public Videojuego(int id, String title, Date release_date, String publisher,
			String system, int rating, int num_players) {
		this.id = id;
		this.title = title;
		this.release_date = release_date;
		this.publisher = publisher;
		this.system = system;
		this.rating = rating;
		this.num_players = num_players;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public Date getRelease_date() {
		return release_date;
	}



	public void setRelease_date(Date release_date) {
		this.release_date = release_date;
	}



	public String getPublisher() {
		return publisher;
	}



	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}



	public String getSystem() {
		return system;
	}



	public void setSystem(String system) {
		this.system = system;
	}



	public int getRating() {
		return rating;
	}



	public void setRating(int rating) {
		this.rating = rating;
	}



	public int getNum_players() {
		return num_players;
	}



	public void setNum_players(int num_players) {
		this.num_players = num_players;
	}
	
	public void actualizar() {
		SessionFactory factoriaSession = HibernateHelper.getSessionFactory();
		Session session = factoriaSession.openSession();
		session.beginTransaction();
		session.saveOrUpdate(this);
		session.getTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Videojuego> buscarTodos()  {
		SessionFactory factoriaSession = HibernateHelper.getSessionFactory();
		Session session = factoriaSession.openSession();
		
		Query query = session.createQuery("SELECT p FROM Videojuego p");
		List<Videojuego> listaDeVideojuegos = query.list();
		System.out.println(listaDeVideojuegos.get(0).getId());
		
		session.close();
		return listaDeVideojuegos;
	}
	
	public static Videojuego buscarPorId(int id)  {
		SessionFactory factoriaSession = HibernateHelper.getSessionFactory();
		Session session = factoriaSession.openSession();
		Videojuego videojuego = (Videojuego) session.get(Videojuego.class,id);
		session.close();
		return videojuego;
	}
	
	//VALIDAR
		public boolean validar(HttpServletRequest request){
			boolean error = false;
			
			if(title.equals("")){
				request.setAttribute("error_editar_titulo", "Introduzca un titulo válido.");
				error = true;
			}
			if(release_date.toString().equals("")){
				request.setAttribute("error_editar_release_date", "Introduzca una fecha válida.");
				error = true;
			}
			if(publisher.equals("")){
				request.setAttribute("error_editar_publisher", "Introduzca una compañia válida.");
				error = true;
			}
			if(system.equals("")){
				request.setAttribute("error_editar_system", "Introduzca una plataforma válida.");
				error = true;
			}
			
			if(String.valueOf(rating).equals("")){
				request.setAttribute("error_editar_rating", "Introduzca una valoración válida.");
				error = true;
			}
			if(String.valueOf(num_players).equals("")){
				request.setAttribute("error_editar_num_players", "Introduzca un número de jugadores válido.");
				error = true;
			}
			return error;
		}
	
}
