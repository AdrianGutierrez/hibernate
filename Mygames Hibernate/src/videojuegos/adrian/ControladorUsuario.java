package videojuegos.adrian;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ControladorUsuario extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher despachador = null;
		HttpSession session = request.getSession(true);
		
		if (request.getServletPath().equals("/usuarioLogout")){
			//El usuario hace logout
			session.invalidate();
			despachador = request.getRequestDispatcher("/usuarioLogin.jsp");
			
		} else if (request.getServletPath().equals("/usuarioLogin")){
			//El usuario intenta acceder al index sin loguearse
			despachador = request.getRequestDispatcher("/usuarioLogin.jsp");
		}
		
		despachador.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession session = request.getSession(true);
		RequestDispatcher despachador = null;
		
		if (request.getServletPath().equals("/usuarioLogin")) {
			//El usuario hace login
			
			String user = request.getParameter("user");
			String pass = request.getParameter("pass");
//			System.out.println(user);
//			System.out.println(pass);

			Usuario usuario = Usuario.buscar(user, pass);
			
			if(usuario != null){
				session.setAttribute("UsuarioNOMBRE", usuario.getUser());

				response.sendRedirect("index.do");
					
			}else{
				request.setAttribute("error_login", "Rellena todos los campos.");
				despachador = request.getRequestDispatcher("/usuarioLogin.jsp");
				despachador.forward(request, response);
			}
		}
	}
}
